package pages.AdminMainMenu;

import org.testng.annotations.Test;

import com.test.NIMS.pages.AdminMainMenuPage;

public class TestAdminMenu {
	
	
  @Test(enabled=false,priority = 2)
  public void testNavigateToMarketingAndSalesService() throws InterruptedException {
	 

	  AdminMainMenuPage.navigateToMarketingAndSales();
		
  }
  
  
  @Test(enabled=true,priority = 21)
  public void testNavigateToPendingSOWWorkHead() throws InterruptedException {
	 

	  AdminMainMenuPage.navigatePendingWorkHead();
		
  }
  
  
  
  @Test(enabled=false,priority = 5)
  public void testClickOnCRMRefNr() throws InterruptedException
  {
	  
	  AdminMainMenuPage.clickOnCRFLink();
  }

}
