package pages.LoginAdmin;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.test.NIMS.data.LoginData;
import com.test.NIMS.pages.LoginPage;

import pages.HomePage.HomePageTitle;

public class TestLoginAdmin {
	
	
    HomePageTitle homePages = new HomePageTitle();
   
  @Test(enabled=true)
  public void testLoginAdmin() throws InterruptedException {
	  
	  
	  homePages.homePageTitleTest();
	  LoginPage.loginAdmin();
	  Assert.assertEquals(LoginData.adminPassword, "nihilent@123");
  }
}
