package pages.LoginPage;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.test.NIMS.data.LoginData;
import com.test.NIMS.pages.LoginPage;

import pages.HomePage.HomePageTitle;

public class TestLogin {
	
     HomePageTitle homePages = new HomePageTitle();
    
	 @Test(enabled=true,priority = 1)
	  public void testLogin() throws InterruptedException {
		 
		  homePages.homePageTitleTest();
		  LoginPage.login();
		  Assert.assertEquals(LoginData.adminPassword, "nihilent@123");
	  }

}
