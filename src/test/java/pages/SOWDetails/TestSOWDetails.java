package pages.SOWDetails;

import java.io.IOException;
import org.testng.annotations.Test;

import com.test.NIMS.data.SOWDetailsData;
import com.test.NIMS.locators.SOWDetailsPageLocators;
import com.test.NIMS.pages.SOWDetailsPage;
import com.test.NIMS.util.Logout;

import junit.framework.Assert;

public class TestSOWDetails {
	
	
	@Test(enabled=false,priority = 6)
	  public void testSOWOpportunityDetails()throws InterruptedException {
		 
		SOWDetailsPage.populateSOWOpportunityDetails();
	  }
	
  @Test(enabled=false,priority = 7)
  public void testSOWDimensionDetails()
		  throws InterruptedException {
	 
	  SOWDetailsPage.populateSOWDimensionDetails();
  }
  
  @Test(enabled=false,priority = 8)
  public void testSOWDetails()throws 
  InterruptedException, IOException {
	 
	  SOWDetailsPage.populateSOWDetails();
  }
  
  @Test(enabled=false,priority = 9)
  public void testApprovals()
		  throws InterruptedException
  {
	  SOWDetailsPage.populateApprovals();
  }
  
  @Test(enabled=false,priority = 10)
  public void testBillingClientInformation()
   throws InterruptedException
  {
	  SOWDetailsPage.populateBillingInformationDetails();
  }
  
  @Test(enabled=false,priority = 11) 
  public void testProjectBillingInformation() 
		  throws InterruptedException
  {
	  SOWDetailsPage.populateProjectInformationDetails();
  }
  
  @Test(enabled=false,priority = 12) 
  public void testTeamSplit()
		  throws InterruptedException
  {
	 
	  SOWDetailsPage.populateteamSplit();
	  
	  
  }
  
  @Test(enabled=false,priority = 13) 
  public void testSubmitSOW()
		  throws InterruptedException
  {
	 
	  SOWDetailsPage.submit();
	  Assert.assertEquals(SOWDetailsData.approvedWorkAheadSuccessMessage1,
			  SOWDetailsPageLocators.getSpanMessages().get(1));
	  
	  
  }
  
  @Test(enabled=false,priority = 14) 
  public void testLogout()
		  throws InterruptedException
  {
	 
	 Logout.logout();
	  
	  
  }
}
