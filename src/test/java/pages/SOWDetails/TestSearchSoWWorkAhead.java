package pages.SOWDetails;

import org.testng.annotations.Test;

import com.test.NIMS.pages.SearchSOWDetailsPage;

public class TestSearchSoWWorkAhead {
	
	@Test(enabled=true,priority = 5)
	  public void searchByProposalNo()throws InterruptedException {
		 
		SearchSOWDetailsPage.searchByProposalNo();
		
	  }
	
	
	
	@Test(enabled=false,priority = 16)
	  public void testSearchByCRMRefNo()throws InterruptedException {
		 
		SearchSOWDetailsPage.searchByCRMRefNo();
		
	  }
	
	@Test(enabled=false,priority = 17)
	  public void testApproveWorkhead()throws InterruptedException {
		 
		SearchSOWDetailsPage.approveWorkHead();
	  }


}
