package com.test.NIMS.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.test.NIMS.base.NIMSBase;
import com.test.NIMS.data.SOWDetailsData;
import com.test.NIMS.locators.SOWDetailsPageLocators;
import com.test.NIMS.util.Utility;

public class ApproveSoWWorkahead {

	public static void approveSoWWorkAHead() throws InterruptedException
	{
		  Thread.sleep(Utility.WAIT_TIME);
		  WebElement tobeClosedImage = SOWDetailsPageLocators.getDatePickers().get(0);
		  tobeClosedImage.click();
		  Thread.sleep(Utility.WAIT_TIME);
		  
		  WebElement tobeClosedDate =
		  NIMSBase.wait.until(ExpectedConditions.
				  visibilityOfElementLocated
		  (By.xpath(SOWDetailsPageLocators.problemStartDate))); 
		  Thread.sleep(Utility.WAIT_TIME);
		  tobeClosedDate.click();			
		  
		  
		  WebElement approverComment =
				  NIMSBase.wait.until(ExpectedConditions.
						  visibilityOfElementLocated
				  (By.xpath(SOWDetailsPageLocators.approverComment))); 
				  Thread.sleep(Utility.WAIT_TIME);
				  approverComment.sendKeys(SOWDetailsData.approverComment);
				  
				  
				  Thread.sleep(Utility.WAIT_TIME);	  
				  
				  WebElement approve =
						  NIMSBase.wait.until(ExpectedConditions.
								  visibilityOfElementLocated
						  (By.xpath(SOWDetailsPageLocators.approve))); 
						  Thread.sleep(Utility.WAIT_TIME);
						  approve.click();
						  
						  Thread.sleep(Utility.WAIT_TIME);	
						  
						  Alert alert = NIMSBase.driver.switchTo().alert();
						  alert.accept();		  
						  
	}
	
	

}
