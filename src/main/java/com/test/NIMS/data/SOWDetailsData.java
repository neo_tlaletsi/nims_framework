package com.test.NIMS.data;

public class SOWDetailsData {
	
	
	    public static String clientPONumber = "EP090";
	    public static String billingDuration = "Weekly";
	    
	    public static String reasonForWorkHeadText = "This is the reason for work head";
	    public static String scopeOrTechnologyText = "Java Development";
	    public static String preparedByText = "Amruta";
	    
	
	    public static String reviewedByText = "Suraj";
	    public static String peakSizeText = "5";
	    
	    //Billing Contact
	    public static String billingNameText = "Johan Kruger";
	    public static String billingDesignationText = "Software Architect";
	    public static String billingContactNumberText = "0828976545";
	    public static String billingEmailText = "johan.kruger@santam.co.za";
	    
	    //Project Contact Text
	    public static String projectNameText = "Suraj Girolkar";
	    public static String projectDesignationText = "Senior Test Engineer";
	    public static String projectContactNumberText = "8530445636";
	    public static String projectEmailText = "suraj.girolkar@nihilent.com";
	    
	    //Team Split Text
	    public static String roleText = "Test Engineer";
	    public static String quantityText = "9";
	    public static String rateText = "12.00";
	    public static String noOfHoursText = "";
	    public static String percentageAllocationText = "";
	    public static String descriptionText = "This is a description for the project";
	    
	    public static String approverComment = "This Request has been approved";
        public static String approvedWorkAheadSuccessMessage1 = "Mail send succesfully.";
        public static String approvedWorkAheadSuccessMessage2 = "Work ahead request approved, notification sent to sales person, notification sent to finance.";
        
	    //assertions
	    //
	    // 
	 
	    //get two of them
	    //assert each message
        //click on it again to verify the change of the status
        
	    
}
