package com.test.NIMS.locators;

import com.test.NIMS.base.NIMSBase;

public class MainMenuPageLocators  extends NIMSBase {
	
	public static String myProfile = "//a[@class='drop has-submenu' and text()='My Profile']";
	public static String myRequest = "//a[text()='My Requests']";
	public static String reportProblem = "//a[text()=' Report Problem ']";

}